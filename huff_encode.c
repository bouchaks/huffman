
#include "arbrebin.h"
#include "bfile.h"
#include "fap.h"
#include "huffman_code.h"
#include <assert.h>
#include <stdio.h>

typedef struct {
    int tab[256];
} TableOcc_t;

struct code_char HuffmanCode[256];
int k=0;
int t[8]={-1};


void ConstruireTableOcc(FILE *fichier, TableOcc_t *TableOcc) {

    int c;

    for (int i = 0; i < 256; ++i)
    {
    	TableOcc->tab[i]=0;
    }

    printf("Programme realise (ConstruireTableOcc)\n");

    c = fgetc(fichier);
    while (c != EOF) {
     	TableOcc->tab[c]++;  
        c = fgetc(fichier);
    };


    int i;
    for (i = 0; i < 256; i++) {
        if (TableOcc->tab[i] != 0)
            printf("Occurences du caractere %c (code %d) : %d\n", i, i,
                   TableOcc->tab[i]);
    }
}

fap InitHuffman(TableOcc_t *TableOcc) {
    fap file_ap = creer_fap_vide();
    for (int i = 0; i < 256; ++i)
    {
    	if (TableOcc->tab[i] > 0)
    	{	
    		Arbre element = NouveauNoeud(NULL,i, NULL); 
    		file_ap=inserer(file_ap, element, TableOcc->tab[i]);
    		
    	}	
    }
    printf("Programme realise (InitHuffman)\n");
    return file_ap;
}

Arbre ConstruireArbre(fap file) {
    Arbre x=ArbreVide();
    Arbre y=ArbreVide();
    Arbre z=ArbreVide();
    int px,py;
   	while(! est_fap_vide(file->prochain)){
   	 file = extraire(file , &x, &px);
   	 file = extraire(file , &y, &py);

   	 z=NouveauNoeud(x,'@',y);
   
   	 file=inserer(file,z,px+py);


   	}
   	file= extraire(file,&x,&px);
    printf("Programme realise (ConstruireArbre)\n");
    return z;
}

int feuille(Arbre a){
	return (EstVide(FilsGauche(a)) && EstVide(FilsDroit(a)));
}

void transfert (int *ts,int *td, int lg){
	for (int i = 0; i < lg; ++i)
	{
		td[i]=ts[i];
	}
}
void ConstruireCode(Arbre huff) {
		
		if (! feuille(huff))
		{
			t[k] = 0;k++;
			ConstruireCode(FilsGauche(huff));

			t[k-1] = 1;
			ConstruireCode(FilsDroit(huff));	
			k--;
		}else{
			HuffmanCode[Etiq(huff)].lg=k+1;
			transfert(t,HuffmanCode[Etiq(huff)].code,k+1);
		}
}

void afficher_tab(Arbre huff){
		printf("%c",Etiq(huff));
			for (int i = 0; i < k; ++i)
			{
				printf("%d",t[i]);
			}
			printf("\n");
		
}	
void Encoder(FILE *fic_in, FILE *fic_out, Arbre ArbreHuffman) {
    
	EcrireArbre(fic_out, ArbreHuffman);

    printf("Programme non realise (Encoder)\n");
}

int main(int argc, char *argv[]) {

    TableOcc_t TableOcc;
    FILE *fichier;
    FILE *fichier_encode;

    fichier = fopen(argv[1], "r");
    /* Construire la table d'occurences */
    ConstruireTableOcc(fichier, &TableOcc);
    fclose(fichier);

    /* Initialiser la FAP */
    fap file = InitHuffman(&TableOcc);
    /* Construire l'arbre d'Huffman */
    Arbre ArbreHuffman = ConstruireArbre(file);
    	
        AfficherArbre(ArbreHuffman);

    /* Construire la table de codage */
    ConstruireCode(ArbreHuffman);

    /* Encodage */
    fichier = fopen(argv[1], "r");
    fichier_encode = fopen(argv[2], "w");

    Encoder(fichier, fichier_encode, ArbreHuffman);

    fclose(fichier_encode);
    fclose(fichier);

    return 0;
}
